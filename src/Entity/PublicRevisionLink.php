<?php
/**
 * @file
 * A public revision link entity type.
 */

namespace Drupal\public_revisions\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the public revision link entity class.
 *
 * @ContentEntityType(
 *   id = "public_revision_link",
 *   label = @Translation("Public revision Link"),
 *   base_table = "public_revision_link",
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "storage_schema" = "Drupal\public_revisions\PublicRevisionStorageSchema",
 *     "form" = {
 *       "generate" = "Drupal\public_revisions\Form\PublicRevisionGenerateForm",
 *       "delete" = "Drupal\public_revisions\Form\PublicRevisionDeleteForm"
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "revision_id" = "revision_id",
 *     "langcode" = "langcode",
 *     "published" = "published"
 *   },
 *   links = {
 *     "generate" = "/node/{node}/revisions/{node_revision}/generate",
 *     "delete-form" = "/node/{node}/revisions/{node_revision}/delete-link"
 *   }
 * )
 */
class PublicRevisionLink extends ContentEntityBase implements ContentEntityInterface, EntityPublishedInterface {

  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['revision_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Revision ID'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Id'))
      ->setDescription(t('The entity Id'))
      ->setRequired(TRUE);

    $fields['entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type Id'))
      ->setDescription(t('The entity type Id'))
      ->setRequired(TRUE);

    $fields['hash'] = BaseFieldDefinition::create('string')
      ->addConstraint('UniqueField')
      ->setLabel(t('Preview hash'))
      ->setDescription(t('A hash that allows any user to view a revision of this entity.'))
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the link was created.'))
      ->setRequired(TRUE);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    return $fields;
  }

}
