<?php
/**
 * @file
 * A class that renders the UI of the available preview links.
 */

namespace Drupal\public_revisions\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\GeneratedLink;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\node\Controller\NodeController;
use Drupal\node\NodeInterface;

/**
 * A class that renders the UI of the available preview links.
 */
class PreviewController extends NodeController {

  /**
   * The node storage instance.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $nodeStorage;

  /**
   * The public_revision_link storage instance.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $publicRevisionStorage;

  /**
   * A node instance.
   *
   * @var \Drupal\node\NodeInterface
   */
  private NodeInterface $node;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    DateFormatterInterface    $date_formatter,
    RendererInterface         $renderer,
    EntityRepositoryInterface $entity_repository
  ) {
    parent::__construct($date_formatter, $renderer, $entity_repository);
    $this->publicRevisionStorage = $this->entityTypeManager()
      ->getStorage('public_revision_link');
    $this->nodeStorage = $this->entityTypeManager()->getStorage('node');
  }

  /**
   * Deny or allow access to the preview link.
   *
   * @param int $node_revision
   *   The node revision id.
   * @param string $hash
   *   The hash string.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   The access result instance.
   *
   * @throws \Exception
   */
  public function access(int $node_revision, string $hash) {
    // Deny if the revision and hash does not exist.
    $node_entity = $this->nodeStorage->loadRevision($node_revision);
    $cache_tag_id = self::getCacheTagId($node_revision);
    if ($node_entity instanceof NodeInterface && !empty($hash)) {
      $conditions = [
        'entity_id' => $node_entity->id(),
        'revision_id' => $node_revision,
        'hash' => $hash,
      ];

      try {
        $previews = $this->publicRevisionStorage->loadByProperties($conditions);

      } catch (\Exception $e) {
        watchdog_exception('public_revisions:access', $e);
        return AccessResult::forbidden();
      }

      $preview_entity = reset($previews);
      $preview = $preview_entity instanceof ContentEntityInterface && $preview_entity->get('hash')->value === $hash;

      if ($preview) {
        return AccessResult::allowed()
          ->addCacheableDependency($node_entity)
          ->addCacheTags([$cache_tag_id]);
      }
    }
    return AccessResult::forbidden()
      ->addCacheableDependency($node_entity)
      ->addCacheTags([$cache_tag_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function revisionOverview(NodeInterface $node): array {
    $this->node = $node;
    $langcode = $node->language()->getId();

    $build['#title'] = $this->getPageTitle();
    $header = [
      $this->t('Revision'),
      $this->t('Preview link'),
      $this->t('Operations'),
    ];

    $rows = [];
    $default_revision = $node->getRevisionId();
    $current_revision_displayed = FALSE;

    foreach ($this->getRevisionIds($node, $this->nodeStorage) as $vid) {
      /**
       * @var \Drupal\node\NodeInterface $revision
       */
      $revision = $this->nodeStorage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)
          ->isRevisionTranslationAffected()) {
        // We treat also the latest translation-affecting revision as current
        // revision, if it was the default revision, as its values for the
        // current language will be the same of the current default revision in
        // this case.
        $is_current_revision = $vid === $default_revision || (!$current_revision_displayed && $revision->wasDefaultRevision());

        if ($is_current_revision) {
          $current_revision_displayed = TRUE;
        }

        $row = [];
        // Add a revision column title.
        $row[] = $this->getRevisionColumnRenderArray($is_current_revision, $vid, $revision);

        // Add a preview link.
        $row[] = $this->addPreviewLink($is_current_revision, $vid);

        if ($is_current_revision && $node->isPublished()) {
          // Don't add preview links to the current published revision.
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];

          $rows[] = [
            'data' => $row,
            'class' => ['revision-current'],
          ];
        }
        else {
          $links = [];

          // Add the generate preview link.
          $links['generate'] = [
            'title' => $this->t('Generate preview link'),
            'url' => $this->getGeneratePreviewUrl($is_current_revision, $vid),
          ];

          // Add delete preview link.
          $this->addDeletePreviewLink($links, $vid);

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];

          $rows[] = $row;
        }
      }
    }

    $build['node_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#attached' => [
        'library' => ['node/drupal.node.admin'],
      ],
      '#attributes' => ['class' => 'node-revision-table'],
    ];

    $build['pager'] = ['#type' => 'pager'];

    return $build;
  }

  /**
   * Get the page title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translatable markup instance.
   */
  private function getPageTitle(): TranslatableMarkup {
    $langname = $this->node->language()->getName();
    $languages = $this->node->getTranslationLanguages();
    $has_translations = (count($languages) > 1);

    if ($has_translations) {
      $title = $this->t(
        '@langname revisions for %title', [
          '@langname' => $langname,
          '%title' => $this->node->label(),
        ]
      );
    }
    else {
      $title = $this->t(
        'Revisions for %title', [
          '%title' => $this->node->label(),
        ]
      );
    }
    return $title;
  }

  /**
   * Get the node revision link.
   *
   * @param bool $is_current_revision
   *   Check if it's the current revision.
   * @param int $vid
   *   The revision ID.
   * @param \Drupal\node\NodeInterface $revision
   *   The revision instance.
   *
   * @return \Drupal\Core\GeneratedLink
   *   The generated link.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function getNodeRevisionLink(bool $is_current_revision, int $vid, NodeInterface $revision): GeneratedLink {
    // Use revision link, to link to revisions that are not active.
    $date = $this->dateFormatter->format($revision->get('revision_timestamp')->value, 'short');
    if (!$is_current_revision) {
      $link = Link::fromTextAndUrl(
        $date, new Url(
          'entity.node.revision', [
            'node' => $this->node->id(),
            'node_revision' => $vid,
          ]
        )
      )->toString();
    }
    else {
      $link = $this->node->toLink($date)->toString();
    }
    return $link;
  }

  /**
   * Get the revision column render array.
   *
   * @param bool $is_current_revision
   *   Check if it's the current revision.
   * @param int $vid
   *   The revision ID.
   * @param \Drupal\node\NodeInterface $revision
   *   The revision instance.
   *
   * @return array
   *   The column render array.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function getRevisionColumnRenderArray(bool $is_current_revision, int $vid, NodeInterface $revision): array {
    $username = [
      '#theme' => 'username',
      '#account' => $revision->getRevisionUser(),
    ];

    $column = [
      'data' => [
        '#type' => 'inline_template',
        '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
        '#context' => [
          'date' => $this->getNodeRevisionLink($is_current_revision, $vid, $revision),
          'username' => $this->renderer->renderPlain($username),
          'message' => [
            '#markup' => $revision->get('revision_log')->value,
            '#allowed_tags' => Xss::getHtmlTagList(),
          ],
        ],
      ],
    ];

    $this->renderer->addCacheableDependency($column['data'], $username);

    return $column;
  }

  /**
   * Get the preview link render array.
   *
   * @param int $vid
   *   The revision id.
   *
   * @return array|array[]
   *   The render array.
   */
  private function getPreviewLinkRenderArray(int $vid): array {
    $row = [];
    $conditions = [
      'entity_id' => $this->node->id(),
      'revision_id' => $vid,
    ];

    try {
      $previews = $this->publicRevisionStorage->loadByProperties($conditions);
    } catch (\Exception $e) {
      watchdog_exception('public_revisions:preview_link', $e);
      return $row;
    }

    if (!empty($previews)) {
      $preview = reset($previews);
      $url = Url::fromRoute(
        'public_revisions.preview_unpublished',
        [
          'hash' => $preview->get('hash')->value,
          'node_revision' => $vid,
          'node' => $this->node->id(),
        ],
        [
          'absolute' => TRUE,
          'language' => $this->node->language(),
        ]
      );

      $span = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $preview->get('hash')->value,
      ];

      $row = [
        'data' => [
          '#type' => 'link',
          '#title' => $this->t('Link'),
          '#url' => $url,
          '#options' => $url->getOptions(),
          '#suffix' => ' [ ' . $this->renderer->render($span) . ' ]',
        ],
      ];
    }

    return $row;
  }

  /**
   * Add a preview link.
   *
   * @param bool $is_current_revision
   *   Check if it's the current revision.
   * @param int $vid
   *   The revision id.
   *
   * @return array|array[]
   *   The link.
   */
  private function addPreviewLink(bool $is_current_revision, int $vid): array {
    if ($is_current_revision && $this->node->isPublished()) {
      return [];
    }

    return $this->getPreviewLinkRenderArray($vid);
  }

  /**
   * Get the generated preview URL.
   *
   * @param bool $is_current_revision
   *   Check if it's the current revision.
   * @param int $vid
   *   The revision ID.
   *
   * @return \Drupal\Core\Url
   *   The url instance.
   */
  private function getGeneratePreviewUrl(bool $is_current_revision, int $vid): Url {
    // Use a different route if the criteria below are true.
    // 1 - It is a default revision.
    // 2 - The node has one revision.
    // 3 - It is the current revision
    // 4 - The node is not published.
    if (($this->node->isDefaultRevision() && ($this->nodeStorage->countDefaultLanguageRevisions($this->node) === 1)) || ($this->node->isDefaultRevision() && !$this->node->isPublished() && $is_current_revision)) {
      // Use a route with a 'view' access. This is because the 'update' access
      // will deny access to a revision that clarifies to the above conditions.
      // see \Drupal\node\Access\NodeRevisionAccessCheck.
      $url = Url::fromRoute(
        'public_revisions.generate_preview_link',
        [
          'node' => $this->node->id(),
          'node_revision' => $vid,
        ]
      );
    }
    else {
      $url = Url::fromRoute(
        'public_revisions.generate_preview_link', [
          'node' => $this->node->id(),
          'node_revision' => $vid,
        ]
      );
    }
    return $url;
  }

  /**
   * Get the preview delete URL.
   *
   * @param int $vid
   *   The revision ID.
   *
   * @return \Drupal\Core\Url
   *   The url instance.
   */
  private function getDeletePreviewUrl(int $vid): Url {
    if ($this->node->isDefaultRevision()) {
      $url = Url::fromRoute(
        'public_revisions.delete_preview_link',
        [
          'node' => $this->node->id(),
          'node_revision' => $vid,
        ]
      );
    }
    else {
      $url = Url::fromRoute(
        'public_revisions.delete_preview_link', [
          'node' => $this->node->id(),
          'node_revision' => $vid,
        ]
      );
    }
    return $url;
  }

  /**
   * Add delete preview link.
   *
   * @param array $links
   *   A list of link arrays.
   * @param int $vid
   *   The revision ID.
   */
  private function addDeletePreviewLink(array &$links, int $vid): void {
    $conditions = [
      'entity_id' => $this->node->id(),
      'revision_id' => $vid,
    ];

    try {
      $preview = $this->publicRevisionStorage->loadByProperties($conditions);
    } catch (\Exception $e) {
      watchdog_exception('public_revisions:delete_preview_link', $e);
    }

    if (!empty($preview)) {
      $links['delete'] = [
        'title' => $this->t('Delete preview link'),
        'url' => $this->getDeletePreviewUrl($vid),
      ];
    }
  }

  /**
   * Get the cache tag id.
   *
   * @param int $vid
   *   The revision id.
   *
   * @return string
   *   The cache tag id.
   */
  public static function getCacheTagId(int $vid): string {
    return 'public_revisions_link:' . $vid;
  }

}
