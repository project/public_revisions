<?php
/**
 * @file
 * A form to generate a preview link.
 */

namespace Drupal\public_revisions\Form;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form to generate a preview link.
 */
class PublicRevisionGenerateForm extends FormBase {

  /**
   * The node revision.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $revision;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * An array of existing previews.
   *
   * @var bool|\stdClass[]
   */
  protected $existingPreviews;

  /**
   * The public_revision_link storage instance.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $publicRevisionStorage;

  /**
   * Constructs a new PublicRevisionGenerateForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->publicRevisionStorage = $entity_type_manager->getStorage('public_revision_link');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'public_revisions_generate_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL, $node_revision = NULL, $langcode = NULL) {
    /**
     * @var \Drupal\node\Entity\Node revision
     */
    $this->revision = $this->nodeStorage->loadRevision($node_revision);

    $conditions = [
      'entity_id' => $node,
      'revision_id' => $node_revision,
    ];

    $this->existingPreviews = $this->publicRevisionStorage->loadByProperties($conditions);

    if (empty($this->existingPreviews)) {
      $form['actions']['generate'] = [
        '#type' => 'submit',
        '#value' => $this->t('Generate'),
        '#name' => 'generate_button',
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
      ];
    }
    else {
      $form['warning'] = [
        '#type' => 'markup',
        '#markup' => '<p>' . $this->t('This revision already has a generated link.') . '</p>',
      ];
    }

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#name' => 'cancel_button',
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggerName = $form_state->getTriggeringElement()['#name'];

    if ('generate_button' === $triggerName) {
      $rand = new Random();

      $link = $this->publicRevisionStorage->create(
        [
          'entity_id' => $this->revision->id(),
          'hash' => $rand->name(69, TRUE),
          'revision_id' => $this->revision->getRevisionId(),
          'entity_type_id' => 'node',
        ]
      );
      $link->save();

      $this->messenger()
        ->addStatus(
          $this->t(
            'The preview link @type %title has been generated.', [
              '@type' => node_get_type_label($this->revision),
              '%title' => $this->revision->label(),
            ]
          )
        );
    }

    $form_state->setRedirect(
      'public_revisions.preview_version_history',
      ['node' => $this->revision->id()]
    );
  }

}
