<?php
/**
 * @file
 * A form to delete a preview link.
 */

namespace Drupal\public_revisions\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\public_revisions\Controller\PreviewController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form to delete a preview link.
 */
class PublicRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The node revision.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected NodeInterface $revision;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The public_revision_link storage instance.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $publicRevisionStorage;

  /**
   * The public revision link instances.
   *
   * @var array
   */
  private array $publicLinks;

  /**
   * Constructs a new PublicRevisionGenerateForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->publicRevisionStorage = $entity_type_manager->getStorage('public_revision_link');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the preview link?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('public_revisions.preview_version_history', ['node' => $this->revision->id()]);
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'public_revisions_delete_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_revision = NULL) {
    $this->revision = $this->nodeStorage->loadRevision($node_revision);

    $this->publicLinks = $this->publicRevisionStorage->loadByProperties(
      [
        'revision_id' => $this->revision->getRevisionId(),
        'entity_id' => $this->revision->id(),
      ]
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cache_tag_id = PreviewController::getCacheTagId($this->revision->getRevisionId());
    $this->publicRevisionStorage->delete($this->publicLinks);

    $this->messenger()
      ->addStatus($this->t('The preview link has been deleted.'));

    $form_state->setRedirect(
      'public_revisions.preview_version_history',
      ['node' => $this->revision->id()]
    );

    Cache::invalidateTags([$cache_tag_id]);
  }

}
