<?php
/**
 * @file
 * PublicRevisionStorageSchema class.
 */

namespace Drupal\public_revisions;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * PublicRevisionStorageSchema class.
 */
class PublicRevisionStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    if ($base_table = $this->storage->getBaseTable()) {
      $schema[$base_table]['indexes'] += [
        'public_revisions__preview_page' => [
          'hash',
          'revision_id',
          'entity_id',
        ],
        'public_revisions__delete' => ['revision_id', 'entity_id'],
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

    if ($table_name == 'public_revision_link') {
      switch ($field_name) {
        case 'revision_id':
        case 'published':
        case 'entity_id':
        case 'hash':
        case 'entity_type_id':
        case 'created':
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;
        case 'revision_id':
          $this->addSharedTableFieldUniqueKey($storage_definition, $schema, TRUE);
          break;
      }
    }

    return $schema;
  }

}
