# Public revisions
The module provides a way to share unpublished revisions (published or unpublished) with anyone via special, unique URLs.

## Usage
1. Visit the ```/node/{node}/revisions/public-revision-links``` path, or click on the 'Public revision links' tab on the node page.
1. Click on the 'Generate preview link' to create the link.
1. Share the generated link.
1. To stop or deny access to the previous shared generated link, simply click on the 'Delete preview link' button.
