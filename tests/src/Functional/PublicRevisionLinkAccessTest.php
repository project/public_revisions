<?php
/**
 * @file
 * Test preview links functionality.
 */

namespace Drupal\Tests\public_revisions\Functional;

use Drupal\node\NodeInterface;
use Drupal\public_revisions\Entity\PublicRevisionLink;
use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Test access to preview pages with valid/invalid hashes.
 *
 * @group public_revisions
 */
class PublicRevisionLinkAccessTest extends NodeTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'public_revisions',
    'datetime',
  ];

  /**
   * A list of nodes created to be used as starting point of different tests.
   *
   * @var Drupal\node\NodeInterface[]
   */
  protected $nodes;

  /**
   * Revision logs of nodes created by the setup method.
   *
   * @var string[]
   */
  protected $revisionLogs;

  /**
   * An arbitrary user for revision authoring.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $revisionUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create and log in user.
    $web_user = $this->drupalCreateUser(
      [
        'view page revisions',
        'revert page revisions',
        'delete page revisions',
        'edit any page content',
        'delete any page content',
      ]
    );
    $this->drupalLogin($web_user);

    // Create an initial node.
    $node = $this->drupalCreateNode();

    // Create a user for revision authoring.
    // This must be different from user performing revert.
    $this->revisionUser = $this->drupalCreateUser();

    $nodes = [];
    $logs = [];

    // Get the original node.
    $nodes[] = clone $node;

    // Create three revisions.
    $revision_count = 3;
    for ($i = 0; $i < $revision_count; $i++) {
      $logs[] = $node->revision_log = $this->randomMachineName(32);

      $node = $this->createNodeRevision($node);
      $nodes[] = clone $node;
    }

    $this->nodes = $nodes;
    $this->revisionLogs = $logs;
  }

  /**
   * Test overview page.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testPublicRevisionsOverviewPage() {
    $nodes = $this->nodes;

    // Get last node for simple checks.
    $node = $nodes[3];

    // Create and log in user.
    $content_admin = $this->drupalCreateUser(
      [
        'view all revisions',
        'revert all revisions',
        'delete all revisions',
        'edit any page content',
        'delete any page content',
      ]
    );
    $this->drupalLogin($content_admin);

    $this->drupalGet("node/" . $node->id() . "/revisions/public-revision-links");
    $this->assertSession()->linkExists('Generate preview link');
  }

  /**
   * Test generated page.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testGeneratePage() {
    $nodes = $this->nodes;

    /** @var \Drupal\node\Entity\Node $node */
    $node = $nodes[2];

    // Create and log in user.
    $content_admin = $this->drupalCreateUser(
      [
        'view all revisions',
      ]
    );
    $this->drupalLogin($content_admin);

    $this->drupalGet('/node/' . $node->id() . '/revisions/' . $node->getRevisionId() . '/generate');
    $this->assertSession()->pageTextContains('Generate a preview link');
    $this->assertSession()->buttonExists('Generate');
  }

  /**
   * Test generated preview link.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testGeneratePreviewLink() {
    /**
     * @var \Drupal\Core\Entity\EntityStorageInterface $public_revisions_storage
     */
    $public_revisions_storage = $this->container->get('entity_type.manager')
      ->getStorage('public_revision_link');
    $nodes = $this->nodes;

    /**
     * @var \Drupal\node\Entity\Node $node
     */
    $node = $nodes[2];

    // Create and log in user.
    $content_admin = $this->drupalCreateUser(
      [
        'view all revisions',
        'revert all revisions',
        'delete all revisions',
        'edit any page content',
        'delete any page content',
      ]
    );
    $this->drupalLogin($content_admin);

    // Generate the preview link.
    $this->drupalGet('/node/' . $node->id() . '/revisions/' . $node->getRevisionId() . '/generate');
    $this->submitForm([], 'Generate');
    $this->assertSession()
      ->pageTextContains("The preview link Basic page {$node->getTitle()} has been generated.");

    $previews = $public_revisions_storage->loadByProperties(
      [
        'entity_id' => $node->id(),
        'revision_id' => $node->getRevisionId(),
      ]
    );

    // Check if the page contains the hash string.
    /**
     * @var \Drupal\public_revisions\Entity\PublicRevisionLink $preview
     */
    $preview = reset($previews);
    $this->assertSession()->pageTextContains($preview->get('hash')->value);

    // Check if the preview link exists.
    $this->assertSession()
      ->linkByHrefExists($this->baseUrl . '/public/' . $node->id() . '/revision/' . $node->getRevisionId() . '/view/' . $preview->get('hash')->value);
  }

  /**
   * Test viewing preview link page.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testViewingPreviewLinkPage() {
    $nodes = $this->nodes;

    /**
     * @var \Drupal\node\Entity\Node $node
     */
    $node = $nodes[2];

    /**
     * @var \Drupal\public_revisions\Entity\PublicRevisionLink $preview
     */
    $preview = PublicRevisionLink::create(
      [
        'entity_id' => $node->id(),
        'revision_id' => $node->getRevisionId(),
        'hash' => $this->randomMachineName(69),
        'entity_type_id' => 'node',
      ]
    );
    $preview->save();

    $this->drupalLogout();

    // Check if the anonymous user can view the page.
    $this->drupalGet('/public/' . $node->id() . '/revision/' . $node->getRevisionId() . '/view/' . $preview->get('hash')->value);
    $this->assertSession()->pageTextContains($node->get('body')->value);
    $this->assertSession()->statusCodeEquals(200);

    // Check that a non-existent hash is denied access on the preview route.
    $this->drupalGet('/public/' . $node->id() . '/revision/' . $node->getRevisionId() . '/view/' . $this->randomMachineName(69));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test that the preview link is deleted.
   *
   * When the corresponding revision is deleted.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testDeleteRevisionWithPreviewLink() {
    /**
     * @var \Drupal\Core\Entity\EntityStorageInterface $node_storage
     */
    $node_storage = $this->container->get('entity_type.manager')
      ->getStorage('node');
    $nodes = $this->nodes;

    /**
     * @var \Drupal\node\Entity\Node $node
     */
    $node = $nodes[2];

    /**
     * @var \Drupal\public_revisions\Entity\PublicRevisionLink $preview
     */
    $preview = PublicRevisionLink::create(
      [
        'entity_id' => $node->id(),
        'revision_id' => $node->getRevisionId(),
        'hash' => $this->randomMachineName(69),
        'entity_type_id' => 'node',
      ]
    );
    $preview->save();
    $preview_id = $preview->id();
    $hash = $preview->get('hash')->value;
    $revision_id = $preview->get('revision_id')->value;
    $nid = $preview->get('entity_id')->value;

    // Delete the revision.
    $node_storage->deleteRevision($node->getRevisionId());

    // Check that the preview is indeed also deleted when the revision is
    // deleted.
    $load_preview = PublicRevisionLink::load($preview_id);
    $this->assertNull($load_preview);

    // Check that indeed the preview link cannot be accessed anymore.
    $this->drupalGet('/public/' . $nid . '/revision/' . $revision_id . '/view/' . $hash);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test that the preview link is deleted.
   *
   * When the corresponding node is deleted.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testDeleteNodeWithPreviewLinks() {
    $nodes = $this->nodes;

    /**
     * @var \Drupal\node\Entity\Node $node
     */
    $node = $nodes[2];

    /**
     * @var \Drupal\public_revisions\Entity\PublicRevisionLink $preview
     */
    $preview = PublicRevisionLink::create(
      [
        'entity_id' => $node->id(),
        'revision_id' => $node->getRevisionId(),
        'hash' => $this->randomMachineName(69),
        'entity_type_id' => 'node',
      ]
    );
    $preview->save();
    $preview_id = $preview->id();
    $hash = $preview->get('hash')->value;
    $revision_id = $preview->get('revision_id')->value;
    $nid = $preview->get('entity_id')->value;

    // Delete the node.
    $node->delete();

    // Check that the preview is indeed also deleted when the revision is
    // deleted.
    $load_preview = PublicRevisionLink::load($preview_id);
    $this->assertNull($load_preview);

    // Check that indeed the preview link does not exist anymore.
    $this->drupalGet('/public/' . $nid . '/revision/' . $revision_id . '/view/' . $hash);
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Creates a new revision for a given node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node object.
   *
   * @return \Drupal\node\NodeInterface
   *   A node object with up to date revision information.
   */
  protected function createNodeRevision(NodeInterface $node) {
    // Create revision with a random title and body and update variables.
    $node->title = $this->randomMachineName();
    $node->body = [
      'value' => $this->randomMachineName(32),
      'format' => filter_default_format(),
    ];
    $node->setNewRevision();
    // Ensure the revision author is a different user.
    $node->setRevisionUserId($this->revisionUser->id());
    $node->save();

    return $node;
  }

}
