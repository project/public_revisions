<?php
/**
 * @file
 * Test preview links with language or translated content.
 */

namespace Drupal\Tests\public_revisions\Functional;

use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\public_revisions\Entity\PublicRevisionLink;
use Drupal\Tests\content_translation\Functional\ContentTranslationPendingRevisionTestBase;

/**
 * Test preview links with language or translated content.
 */
class PublicRevisionLinkTranslationTest extends ContentTranslationPendingRevisionTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'public_revisions',
    'content_translation',
    'contextual',
    'language',
    'content_moderation',
  ];

  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'node';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->enableContentModeration();
  }

  /**
   * Test preview links with language or translated content.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testPreviewLinkWithLanguage() {
    $this->drupalLogin($this->rootUser);

    // Create a test node.
    $values = [
      'title' => "Test 1 EN",
      'moderation_state' => 'published',
    ];
    $id = $this->createEntity($values, 'en');
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->storage->load($id);

    // Add a draft translation and check that it is available only in the latest
    // revision.
    $add_translation_url = Url::fromRoute("entity.{$this->entityTypeId}.content_translation_add", [
      $entity->getEntityTypeId() => $id,
      'source' => 'en',
      'target' => 'it',
    ],
      [
        'language' => ConfigurableLanguage::load('it'),
        'absolute' => FALSE,
      ]
    );

    $this->drupalGet($add_translation_url);
    $edit = [
      'title[0][value]' => "Test 2 IT",
      'moderation_state[0][state]' => 'draft',
    ];
    $this->submitForm($edit, 'Save (this translation)');

    $entity = $this->storage->loadUnchanged($id);
    $this->assertFalse($entity->hasTranslation('it'));

    $it_revision = $this->loadRevisionTranslation($entity, 'it');
    $this->assertTrue($it_revision->hasTranslation('it'));


    /**
     * @var \Drupal\public_revisions\Entity\PublicRevisionLink $preview
     */
    $preview = PublicRevisionLink::create(
      [
        'entity_id' => $it_revision->id(),
        'revision_id' => $it_revision->getRevisionId(),
        'hash' => $this->randomMachineName(69),
        'entity_type_id' => 'node',
        'langcode' => 'it',
      ]
    );
    $preview->save();

    // Check that a preview link works on the default language.
    $this->drupalGet('/public/' . $preview->get('entity_id')->value . '/revision/' . $preview->get('revision_id')->value . '/view/' . $preview->get('hash')->value);
    $this->assertSession()->statusCodeEquals(200);

    // Check that a preview link of an existing language can be viewed.
    $this->drupalGet('/' . $preview->get('langcode')->value . '/public/' . $preview->get('entity_id')->value . '/revision/' . $preview->get('revision_id')->value . '/view/' . $preview->get('hash')->value);
    $this->assertSession()->statusCodeEquals(200);

    // Check that a preview link of a non enabled language cannot be viewed.
    $this->drupalGet('/nl/public/' . $preview->get('entity_id')->value . '/revision/' . $preview->get('revision_id')->value . '/view/' . $preview->get('hash')->value);
    $this->assertSession()->statusCodeEquals(404);
  }

}
